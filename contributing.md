---
layout: page
title: Contributing
permalink: /contributing/
sitemap: true
---

Kaidan is open-source software and everybody is welcome to contribute to it.

Developers can start with our [project site][repository] and help us with [easy tasks][issues-junior-job] to get to know Kaidan's source code.
More experienced developers might have a look at the [medium-priority issues][issues-medium-priority] or [create a new feature][merge-requests].

But even if you are not a developer, you can contribute to Kaidan by [submitting bug reports][issues], [adding translations][translations] or simply spreading the word!

There is also an overview of [supported XEPs and RFCs][supported-xeps].

[issues]: https://invent.kde.org/network/kaidan/-/issues
[issues-junior-job]: https://invent.kde.org/network/kaidan/-/issues?state=opened&label_name[]=Junior%20Job
[issues-medium-priority]: https://invent.kde.org/network/kaidan/-/issues?state=opened&label_name[]=priority%3Amedium
[merge-requests]: https://invent.kde.org/network/kaidan/-/merge_requests
[repository]: https://invent.kde.org/network/kaidan
[supported-xeps]: https://invent.kde.org/network/kaidan/-/wikis/xeps-rfcs
[translations]: https://hosted.weblate.org/projects/kaidan/translations/
