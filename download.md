---
layout: page
title: Download
permalink: /download/
sitemap: true
---

### Stable release
<a class="btn btn-primary btn-sm" href="{{ "/2020/08/20/kaidan-0.6.0/#download" | prepend: site.baseurl }}">
	<img style="border: none" src="{{ "/images/arrow-right.svg" | prepend: site.baseurl }}" width="25" height="25" />
	Get the latest release
</a>

### Nightly builds
#### Linux

 * [Nightly Flatpak](https://invent.kde.org/network/kaidan/-/wikis/using/flatpak)

#### Android (experimental)

* [KDE Nightly F-Droid repository](https://community.kde.org/Android/FDroid)
* [APK](https://binary-factory.kde.org/job/Kaidan_android/lastSuccessfulBuild/artifact/kaidan_build_apk-debug.apk)

### Source code
Kaidan's source code can be found at [invent.kde.org](https://invent.kde.org/network/kaidan).
