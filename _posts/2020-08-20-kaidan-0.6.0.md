---
layout: post
title: "Kaidan 0.6.0 released"
date: 2020-08-20 23:00:00 +02:00
author: lnj
---

After some months now we're proud to present our next release providing
mostly stability fixes and some smaller new features. This includes an offline
message queue and an overhauled message correction.

The flatpak builds are working again and we've been working on the Windows
builds and macOS builds too (especially JBB). We hope that we can provide
Windows and macOS builds again in the next weeks in form of a patch release.

![screenshot of Kaidan 0.5]({{ "/images/screenshots/screenshot-0.5.png" | prepend: site.baseurl }})

### Offline message queue

When you want to write a message while you're offline, the message is now
cached and sent when Kaidan is connected again.

![A message in the offline quque]({{ "/images/screenshots/2020-08-20-offline-queue.png" | prepend: site.baseurl }})

Three dots appear when the message hasn't been sent yet. Wait until Kaidan has
reconnected.

![Message is sent now]({{ "/images/screenshots/2020-08-20-offline-queue-sent.png" | prepend: site.baseurl }})

A grey checkmark will appear as soon as the message has been sent to the
server.

![Message has been received now]({{ "/images/screenshots/2020-08-20-offline-queue-received.png" | prepend: site.baseurl }})

And finally, when your chat partner receives the message, the checkmark will
become green.

## Changelog

Here is a list with all changes in detail.

These are the new features of the release:
 * When offline, messages are cached now to be sent later (yugubich)
 * It's allowed to also correct other messages than the last one now (yugubich)
 * Also pending (unsent) messages can be corrected now (yugubich)
 * Chats can be opened from the notifications now (melvo, jbb, cacahueto)
 * New option to permanently hide your password in Kaidan (melvo)
 * New buttons for easily copying your JID and password (jbb, fazevedo)
 * Moved account management pages into the settings (jbb)
 * The cursor is moved to the end of the text field when correcting a message
   now (melvo)
 * Scanning QR codes without a password works now and results in only the JID
   being set (melvo)
 * The roster is called contact list now (jbb)
 * The resource for the displayed presence is picked with fixed rules now (it
   was random before which resource is displayed) (lnj)
 * Handle notifications differently on GNOME to keep them in the notifications
   area (melvo)
 * Switched to the upstream HTTP File Upload implementation (lnj)
 * Code refactoring and partial rewrite of the following classes: Kaidan,
   ClientWorker, RosterManager, PresenceCache, DownloadManager, TransferCache,
   QrCodeDecoder (lnj, jbb)

The following bugs have been fixed:
 * Playback issues in media video preview (fazevedo)
 * Messages sent from other of your devices are displayed as they were sent by
   the chat partner (lnj)
 * Notifications are shown persistently on the screen (jbb)
 * Roster item names are not updated in the database (melvo)
 * Roster items are not updated in the model correctly (melvo)
 * All sheets contain two headers: It uses the new built-in header property now
   (jbb)
 * Unreadable buttons with white text on white background in some styles (jbb)
 * Database version isn't saved correctly (melvo)
 * Errors when building with newer ZXing versions (vkrause)

Note: Kaidan requires a C++17-compliant compiler now when building from
sources.

## Download

* [Source code (.tar.xz)](https://download.kde.org/unstable/kaidan/0.6.0/kaidan-0.6.0.tar.xz) ([sig](https://download.kde.org/unstable/kaidan/0.6.0/kaidan-0.6.0.tar.xz.sig) signed with [4663231A91A1E27B](https://keys.openpgp.org/vks/v1/by-fingerprint/03C2D10DC97E5B0BEBB8F3B44663231A91A1E27B))
* [Android (arm) (not signed)](https://download.kde.org/unstable/kaidan/0.6.0/kaidan-0.6.0.arm.apk)
* [Android (arm64) (not signed)](https://download.kde.org/unstable/kaidan/0.6.0/kaidan-0.6.0.arm64.apk)
* [Linux (Flatpak on Flathub)](https://flathub.org/apps/details/im.kaidan.kaidan)
* [Linux (AppImage)](https://download.kde.org/unstable/kaidan/0.6.0/kaidan-0.6.0.x86_64.AppImage)

Nightly builds are available on our [downloads]({{ "/download/" | prepend: site.baseurl }}) page as usual.

