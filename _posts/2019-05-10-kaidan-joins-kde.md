---
layout: post
title: "Kaidan joins KDE"
date: 2019-05-10 20:02:00 +02:00
author: jbb
---

![screenshot of Kaidan on KDE Plasma desktop environment]({{ "/images/screenshot-kde.png" | prepend: site.baseurl }})

Kaidan is a simple, user-friendly Jabber/XMPP client providing a modern user interface based on Kirigami and QtQuick.
Kaidan aims to become a full-featured alternative to proprietary messaging solutions like Telegram, but featuring decentralization and privacy.

The Kaidan team has always had a good relationship with the KDE community, our code uses KDE's [Kirigami](https://kde.org/products/kirigami/) framework and is written with [Plasma Mobile](https://www.plasma-mobile.org) in mind.
So we decided it is only logical for us to join KDE officially, and we are happy to announce that we now finally did.

This introduces some changes and benefits to the development workflow. First of all, we can now use KDE's GitLab instance, [KDE Invent](https://invent.kde.org/), which eliminates the need for hosting our own.
In the future, we plan to provide official Windows and macOS builds using KDE's Binary Factory infrastructure.

If you are not a user of the KDE Plasma desktop environment, you might wonder whether this decision will influence our support for other desktop environments, like GNOME, but we will continue to aim at the full range of possible platforms, like we always did, regardless of this decision.
Of course, merge requests improving the experience on other desktop environments are also very welcome! If you want to contribute, you can now do this on [invent.kde.org/KDE/kaidan](https://invent.kde.org/KDE/kaidan).
This requires the use of a [KDE Identity account](https://identity.kde.org/) but is otherwise not different from our own GitLab instance you might be used to.

Happy messaging!
