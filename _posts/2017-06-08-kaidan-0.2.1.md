---
layout: post
title: "Kaidan 0.2.1 released"
date: 2017-06-08 00:00:00 +02:00
author: lnj
---

## Changelog

Fixes:
 * Roster page: Fixed style: Now has contour lines and a cool material effect (LNJ)

**Download**: [kaidan-v0.2.1.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.2.1/kaidan-v0.2.1.tar.gz)
