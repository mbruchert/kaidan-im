---
layout: post
title: "Kaidan 0.5.0 released!"
date: 2020-04-06 01:00:00 +02:00
author: zatrox
---

![screenshot of Kaidan 0.5]({{ "/images/screenshots/screenshot-0.5.png" | prepend: site.baseurl }})

# It's done - Kaidan 0.5.0 is here!

After more than half a year the next release is here, but the waiting was worth it!
It includes the all new onboarding, which aims at better usability for new XMPP users and improved security, while minimizing additional effort by the user.
For further information look at the [blog post]({{ "/2020/01/08/Easy-Registration/" | prepend: site.baseurl }}) dedicated to this topic.

And even more!
Now recording and sending audio and video is possible with Kaidan, as well as searching for contacts and messages.
Additionally, many smaller features and fixes are included in this release.
But have a look at the changelog yourself.

We sadly have to inform you that we encountered difficulties building Kaidan for Windows and building the Flatpak as one option to use Kaidan on Linux.
But we are already working on fixing it and Kaidan 0.5 will hopefully be available on Windows and as a Flatpak for Linux soon™.

At the current state, it is possible to show the password with two clicks.
That issue is also going to be addressed in the future.

We like to thank the [DBJR (German Federal Youth Council)](https://tooldoku.dbjr.de) for supporting us financially and technically.

We would be pleased about [bug reports](https://invent.kde.org/KDE/kaidan/issues), [translations](https://hosted.weblate.org/projects/kaidan/translations/) and iOS developers and testers ([contact us](https://i.kaidan.im)) to help us improve Kaidan.

## Changelog

Features:
 * Add parsing of XMPP URIs (lnj, melvo)
 * Add QR code scanning and generation (lnj, jbb, melvo)
 * Add contact search (zatrox, lnj)
 * Add muting notifications for messages of contacts (zatrox)
 * Add renaming contacts (lnj, zatrox, melvo)
 * Show user profile information (lnj, jbb)
 * Add extended multimedia support (fazevedo)
 * Add message search (blue)
 * Redesign contact list, text avatar, counter for unread messages, chat page, chat message bubble (melvo)
 * Show notifications on Android (melvo, jbb, cacahueto)
 * Add option for enabling or disabling an account temporarily (melvo)
 * Refactor login screen with hints for invalid credentials and better usage of keyboard keys (melvo)
 * Add message quoting (jbb)
 * Truncate very long messages to avoid crashing Kaidan or using it to full capacity (jbb)
 * Add button with link for issue tracking to about page (melvo)
 * Improve messages for connection errors (melvo)
 * Add account deletion (melvo, mbb)
 * Redesign logo and global drawer banner (melvo, mbb)
 * Add onboarding with registration, normal login and QR code login (melvo, lnj, jbb, mbb)
 * Add OARS rating (nickrichards)
 * Add secondary roster sorting by contact name (lnj)
 * Add support for recording audio and video messages (fazevedo)
 * Add Kaidan to KDE's F-Droid repository (nicolasfella)
 * Improve build scripts for better cross-platform support (jbb, cacahueto, lnj, mauro)
 * Refactor code for better performance and stability (lnj, jbb, melvo)
 * Add documentation to achieve easier maintenance (melvo, lnj, jbb)

Bugfixes:
 * Fix AppImage build (jbb)
 * Fix scrolling and item height problems in settings (jbb)

Notes:
 * Require Qt 5.12 and QXmpp 1.2
 * Drop Ubuntu Touch support due to outdated Qt

## Download

* [Source code (.tar.xz)](https://download.kde.org/stable/kaidan/0.5.0/kaidan-0.5.0.tar.xz) ([sig](https://download.kde.org/stable/kaidan/0.5.0/kaidan-0.5.0.tar.xz.sig) signed with [AE08C590A7D112C1979D068B04EFAD0F7A4D9724](https://keys.openpgp.org/vks/v1/by-fingerprint/AE08C590A7D112C1979D068B04EFAD0F7A4D9724))
* [macOS (x64)](https://download.kde.org/stable/kaidan/0.5.0/kaidan-0.5.0.x86_64.macos.tar)
* [Android (armv7) (experimental!)](https://download.kde.org/stable/kaidan/0.5.0/kaidan-0.5.0.armhf.apk)
* [Linux (AppImage)](https://download.kde.org/stable/kaidan/0.5.0/kaidan-0.5.0.x86_64.AppImage) (It is possible that the account registration is currently not working. Prefer other ways of using Kaidan.)

Nightly builds are available on our [downloads]({{ "/download/" | prepend: site.baseurl }}) page as usual.

